import React, { Component } from 'react'
import MyTable from './components/MyTable'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container,Row } from 'react-bootstrap';
import Myaccount from './components/Myaccount';

export default class App extends Component {
  constructor(props){
    super(props)
    this.state={
    myuser:[
      {
          id:1,
          name: "virus",
          email:"virus@gmail.com",
          gender:"male",
          isActive: null
      },
      {
          id:2,
          name: "trojan",
          email:"trojan12@gmail.com",
          gender:"female",
          isActive: null
      },
      {
          id:3,
          name: "malware",
          email:"malware@gmail.com",
          gender:"male",
          isActive: null
      },
  ]
  }
  }
  insertuser = (nus) => {
    let tmp=[...this.state.myuser]
    tmp.push(nus);
    this.setState({
      myuser: tmp
    })
}
  render() {
    return (
        <Container>
          <Row>
          <Myaccount myuser={this.state.myuser} insertuser={this.insertuser}/>
          <MyTable myuser={this.state.myuser} /> 
          </Row>
        </Container>
    )
  }
}
