
import React, { Component } from 'react'
import {Form, Button, Col, Image} from 'react-bootstrap'

export default class Myaccount extends Component {
    constructor() {
        super()
        this.state = {
          id: 0,
          name: "",
          email: "",
          gender: "",
          password: "",
        }
      }

      texthandle=(e)=>{
        this.setState({
            [e.target.name] : e.target.value
        },()=>{
            console.log(this.state);
            if(e.target.name==="email"){
                let pattern=/^\S+@\S+\.[a-z]{3}$/g;
                let result= pattern.test(this.state.email.trim());
                if(result){
                    this.setState({
                        emailerr:""
                    })
                }else if(this.state.email===""){
                    this.setState({
                        emailerr:"cannot empty"
                    })
                }else {
                    this.setState({
                        emailerr:"Invalid"
                    })
                }
            }
            if(e.target.name==="password"){
                let patterns=/^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{8,}$/g;
                let results= patterns.test(this.state.password);
                if(results){
                    this.setState({
                        passworderr:""
                    })
                }else if(this.state.password===""){
                    this.setState({
                        passworderr:"cannot empty"
                    })
                }else {
                    this.setState({
                        passworderr:"Invalid"
                    })
                }
            }
            
        })
    }
    tethandle2=(e)=>{
        console.log("read state 1",this.state);
        let index = this.props.myuser.length
        let a=this.props.myuser[index-1].id
          this.state.id = a + 1
        e.preventDefault();
        this.props.insertuser(this.state);
    }
    btnvalidate=()=>{
        if(this.state.emailerr===""&this.state.passworderr===""&&this.state.email !== "" && this.state.password !== "" && this.state.username !== "" && this.gender !== ""){
            return false
        }else
        return true
    }
    render() {
        return (
            <Col xs="5">
            <Form onSubmit={this.tethandle2} style={{width:"80%",marginTop:"50px"}}>
               <Image style={{ margin: "10px 130px" }} width="100px" height="100" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAe1BMVEX///8AAAA+Pj78/Pzv7+/09PT5+flycnKtra3ExMSdnZ319fU1NTVRUVFlZWXi4uKlpaXj4+OTk5MSEhJvb2+4uLh7e3u+vr5aWlocHBzc3NzR0dHLy8uFhYWYmJhmZmYrKyskJCRMTExDQ0OMjIx/f385OTkWFhYjIyPGvM0sAAAHkklEQVR4nO2d6XbqOgxGDySMBcoMhQ5AKeX9n/CUcjm3+uyExJZsdy3t38WRE1vWZPXPH0VRFEVRFEVRFEVRFEVRFEVRFEVRFEVRgNZ29drprIfD4brTmS62eWyBWNmMhw2TQ38bWzAW8u6bZXY3RuNf/i2zXtn0/pvkqh1bTGey/tPd+V34fM1ii+pE67XS9K5Mf+FiHdeY34Xf9h23p5oTbDR2z7GFrkG7U3t+F0YPsQWvyraagrHwSw7IujvwJ6+xha/C/ROwjHXyh2O76TXBL4XTij2FcvKB5wQbjfNj7EmU8bgvEf29cxwver3eYjxdvpf83WwTexrF5IVKdNjf0BM924zXhXOcRJL/Lu2zXeBD164+soXNqbp8xUT3YnawijsvW3QTu23wnqZGnVvnd2/FTZa2n42CSFyTrkXQXRUjZWPTv31xeWuzsYg5rfjbvuW3yRlw7b0pZHVnYftp/PgpNTP8xRBxUMepbZmmUEdMVie2hoDDmiOMPJZACAwbpb4yNM6Nk4Cczhh69OAwiGHjpKRPjdfvFHQxLIZ0Qjeo7WdukbP2DsY5MsvpTIYGd89xIENfpWKf4i58cR5pCiONGaX0IAOr6+wxFgy1T8MCx7Xluka5x+Ljgwrl5xbAqThnktEPeO1+MYhHGC2FZbrg/ITGR+yyyOgHOL6+Xg98xDWLjH5QiXwU6RXwMhgk9ATeuf8JBqdrfE8YtqF/pvOB+5X5QtMULj4FQn2M+BuRbhsOW5lmxwcMI3rRolY3h19OY1qOfgofEGLjECenYanYWYxnuqRYTBAaEllxDOkBTfjyqAUalIqdFqYeHU8EkEYm3b1NHqg0PGEHGhRZsozpDs2r8ETHqFUTOzJMXQEeT6BHxoztIlLPYsEy5nPCM+T5hqukZii/D2PPkAZpJHRpbE1Dz0Oe903fWuzzkK6ouik1O9R9ip2foXpvx5G4zahDxqOf3QHfgqPWJ6dDxvYtwNXhiFFD3Dt6gRQtd+ZQC1R5Rffx4chvMoxIS8Hix2kg1ua/pmAbxo+1garx91ehiDp+vBRi3jPv8SDVzSChL5Bc8422gSZNoYSPenPeZg1UDsU+7y9kVCTPj4gFgElUt8Ey9fuI8Alju05XMPfus7BWMFYitW1Y1OYeFsbKHLfaKn6wnsZ9acGCT+C4v9LGmijXcA2u0XRq9o27XG4ezwSHiR3Q/wGK9uSSg2oZFe2J7MILC5Rt5yCcUQidQqXJP4zq1/e6R7V5H4UjY86HsYUap3oLNTfvesUOXwDm1fRzHVdxYt4qSaZ89gbW9zbqGDfGPv46KRJSM1ew5O7CvNp59mC7+pTYGr1g+Q6NWRV1uLDdWkxKj944WgRtHO7ZzlvrHcTYofwC7JdCT2Ux1K39uixPdoAfLPi+sT/aN9VkWvCDcwp1s1ZMu+tGs7OiV7Qfe8W3nfexy6BKaJU1/JgNRi/9brfbf3kbzEr+bpCMR2EjK7uGXg2euio5vKfYTCL2VIp517IOy+RMGQvoqdchhfBoBSauK7UZPVdYFdcONdPElcyNsXOLocbsF8yxjRfs6vKRuC5dmBfrazNOWJ0++5/3FwaxC5+LcOxhZmOdpGW6KrM1a5OeB/xg7d3iQWqfcWMJRJnsB8PRaDQcVPrjWSKZtSu23jQ/hT28dHv0m+S97nF4R/FWbf4iT1amYp7W4+KCkc14XdBb6ptUGinm9v5Q3yyf77mzra21jdKVUxKGar4vku+wqGaDtVfFjc0SaE9n6550/Xx1lGFe6FVG1ze9AsGOdZV9Pi04TyMbOGZvIbf5XWgVGO1Rp2if4MFVP+T2/RhxoVonePaJRPSsxkC0KU5sO2fp58NmVpUTKRFlDeL7W8y2QNY5TpDYctAPOI6v3NLGNkrfL6xf+mLO5J1bhn7jGbkOloQoX9LPcm4ELwAziy9YK5gs8cjQ2saMyPB65aa+cSlC8sDMaHOHHUxzMOg1PdPc5r9hZnrVIQ9+Q59LlPcYfVt34QLihh6QuTNvbIVgZVI5PpnjtpMNwxAPpU+N/w0gFfh7QMM3UBWKoWbkGuMZzksYZYNFTJK7A0se3wWf9Y9neKjUJryC5n0Ihx+fKbv70TqUfZ/f4Nb4EH4eGuHy3TBBkYpficDWu+K3ZvEslM+CoQ0uHSOGMEoI3xt0t7QBDi80RIUPehmy1iksGf9bv1UAO192Y6xDPuwGvFZR0w30jH+z0mpA2FIy5QZeaai7ZfBYyaAU3NINldyDpSO4TFsBNwQB3qxcmQZYbOFqXkDXyFluYCOGq3hpU1dY7r4JPZhCXvugy3Qv9RjonhAyzg45BClzH7ZhyDA7aFOpjQjHktBT7IRZPrS0J2zLCppwk+rpQn3RsPWRdCN+Cj2FrpSw7Y0ghCmjaiYhHlIE9KKX0XJUlc7C3huA/1wnExmmvnbofgDUM5WJLdCMU+i2ohKNUsufEfouskSjVITWqocuUab5UpkDkcaCQzdOpQaVTFyY7vXQFyKoiyjj19DIbOiqT5ryktHk81Pzfyr9H1xOHnc/nn6KUASmKIqiKIqiKIqiKIqiKIqiKIqiKIqiKPL8Bc6bT1yPSGcpAAAAAElFTkSuQmCC" />
               <h2 style={{ textAlign: "center" }}>Create Account</h2>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Username</Form.Label>
              <Form.Control name="name" onChange={this.texthandle} type="text" placeholder="username" />
              <Form.Text className="text-muted">
                <p></p>
              </Form.Text>
              <Form.Label>Gender</Form.Label>
            <br />
            <Form.Check
               custome
               inline
               label="Male"
               type="radio"
               value="male"
               name="gender"
               checked={this.state.gender== "male"}
               onChange={(e) => this.setState({ gender: e.target.value })}></Form.Check>
             
            <Form.Check
            custome
            inline
            label="Female"
            type="radio"
            value="female"
            name="gender"
            checked={this.state.gender === "female"}
            onChange={(e) => this.setState({ gender: e.target.value })}
            />
            </Form.Group >
            <Form.Group controlId="formBasicEmail" style={{ marginTop: "12px" }}>
              <Form.Label>Email</Form.Label>
              <Form.Control name="email" onChange={this.texthandle} type="email" placeholder="email" />
              <Form.Text className="text-muted">
              <p style={{color:"red"}}>{this.state.emailerr}</p>
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Password</Form.Label>
              <Form.Control name="password" onChange={this.texthandle} type="password" placeholder="password" />
              <Form.Text className="text-muted">
              <p style={{color:"red"}}>{this.state.passworderr}</p>
              </Form.Text>
            </Form.Group>
            
            <Button disabled={this.btnvalidate()} variant="primary" type="submit">
              Save
            </Button>
            <Button onClick={this.props.onClear} variant="warning" type="button" style={{marginLeft:"10px"}}>{' '}
              Clear
            </Button>
          </Form>
          </Col>
        )
    }
}
