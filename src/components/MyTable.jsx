
import React, { Component } from 'react'
import {Col, Table} from 'react-bootstrap'

export default class MyTable extends Component {
    render() {
        // console.log("props : ",this.props.myuser);
        return (
            <Col xs="5">
            <Table style={{width:"100vh",marginTop:"190px"}} striped bordered hover>
            <thead>
                <tr>
                <th>#</th>
                <th>Username</th>
                <th>Email</th>
                <th>Gender</th>
                </tr>
            </thead>
            <tbody>
                {
                this.props.myuser.map((item,index)=>(
                    <tr>
                <td key={index}>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>{item.gender}</td>
                </tr>
                ))
    }
            </tbody>
            </Table>
            </Col>
        )
    }
}

